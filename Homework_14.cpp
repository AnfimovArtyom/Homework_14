﻿#include <iostream>
#include <string>

int main()
{
	std::string Word = "Skillbox";
	std::cout << Word << "\n";
	std::cout << "Number of characters: " << Word.length() << "\n";
	std::cout << "The first letter of the word: " << Word[0] << "\n";
	std::cout << "The last letter of the word: " << Word[Word.length()-1] << "\n";
	return 0;
}
